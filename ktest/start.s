section .bss

end_of_stack:
resb 8192
stack_space:

section .text

extern kmain

global start
start:
	cli 	
	mov esp, stack_space
    push ebx
    xor ebp, ebp
	call kmain