struct __attribute__((packed))
boot_struct{
    unsigned char has_been_passed;
};

#define OFFWRITE 0

void kmain(struct boot_struct* param) {
    char* video_memory = (char*)0xb8000;
    *video_memory = 'X';
    for(int i = 1; i < 20; i++){
        video_memory[i*2+OFFWRITE] = param->has_been_passed;
    }
    for(int i = 20; param->has_been_passed && i < 40; i++){
        video_memory[i*2+OFFWRITE] = 'a';
    }
    while(1);
}
