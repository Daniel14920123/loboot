BOOTLOADERFOLDER := bootloader
KERNELFOLDER := ktest

BOOTLOADER := $(BOOTLOADERFOLDER)/loboot.bin

CFILES := $(shell find ./$(KERNELFOLDER) -type f -name '*.c')
SFILES := $(shell find ./$(KERNELFOLDER) -type f -name '*.s')
OBJ    := $(SFILES:.s=.o)
OBJ    += $(CFILES:.c=.o)

LINKSCRIPT := $(shell find ./$(KERNELFOLDER) -type f -name 'link.ld')

GCCFLAGS = -std=gnu99 -fno-stack-protector -m32 -O0 -ffreestanding

RM = rm -rf

all: run

run: os-image.bin
	qemu-system-i386 -fda $<

os-image.bin: $(BOOTLOADER) kernel.elf
	cat $^ > $@

kernel.elf: $(LINKSCRIPT) $(OBJ)
	ld -m elf_i386 -o $@ -T $< $(OBJ) --oformat binary

%.o: %.s
	nasm $< -f elf -o $@

%.o: %.c
	gcc $(GCCFLAGS) -c $< -o $@

$(BOOTLOADER):
	$(MAKE) -C $(BOOTLOADERFOLDER)

clean:
	$(MAKE) clean -C $(BOOTLOADERFOLDER)
	$(RM) *.bin *.o *.dis $(OBJ) *.elf